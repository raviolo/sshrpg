#include "ssh_serv.h"

#include <libssh/callbacks.h>
#include <libssh/server.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/param.h>


/* make the user add attachment to channel */
struct sshclient {
  ssh_channel channel;
  void* attachment;
  const char* term; 
};

static bool (*user_login)(struct sshclient*, const char* user, const char* pass);
static void (*user_logout)(struct sshclient*);
static bool (*resize_handler)(struct sshclient*, int cols, int rows);

#define INI_NCLIENTS 16
struct {
  bool terminated;
  pthread_t listen_tid;
  pthread_mutex_t mutex;
  ssh_event resize_event;
  size_t nclients;
  struct sshclient *clients;
  struct ssh_channel_callbacks_struct *callbacks;
} clientPool;

struct session_data_struct {
  struct sshclient client;
  int auth_attempts;
  bool authenticated;
  bool shellreq;
};

static int auth_password(ssh_session session, const char *user, const char *pass, void *userdata) {
  struct session_data_struct *sdata = userdata;
  
  if (user_login(&sdata->client, user, pass)) {
    sdata->authenticated = true;
    return SSH_AUTH_SUCCESS;
  }
  
  sdata->auth_attempts++;
  return SSH_AUTH_DENIED;
}

static ssh_channel channel_open(ssh_session session, void *userdata) {
    struct session_data_struct *sdata = (struct session_data_struct *) userdata;
    sdata->client.channel = ssh_channel_new(session);
    return sdata->client.channel;
}

static int pty_request(ssh_session session, ssh_channel channel, const char *term, int cols, int rows, int py, int px, void *userdata) {
  struct session_data_struct *sdata = userdata;
  resize_handler(&sdata->client, rows, cols);
  sdata->client.term = strdup(term);
  return SSH_OK;
}

static int shell_request(ssh_session session, ssh_channel channel, void *userdata) {
    struct session_data_struct *sdata = userdata;
    sdata->shellreq = true;
    return SSH_OK;
}

static int pty_resize(ssh_session session, ssh_channel channel, int cols, int rows, int py, int px, void *userdata) {
  bool resized = resize_handler(userdata, rows, cols);
  if(resized)
    return SSH_OK;
  return SSH_ERROR;
}

static void* polling_thread(void *userdata) {
  struct session_data_struct sdata = {};
  struct ssh_server_callbacks_struct server_cb = {
    .userdata = &sdata,
    .auth_password_function = auth_password,
    .channel_open_request_session_function = channel_open,
  };
  struct ssh_channel_callbacks_struct channel_cb = {
    .userdata = &sdata,
    .channel_pty_request_function = pty_request,
    .channel_shell_request_function = shell_request,
  };
  ssh_session session = userdata;
  ssh_set_auth_methods(session, SSH_AUTH_METHOD_PASSWORD);
  ssh_callbacks_init(&server_cb);
  ssh_callbacks_init(&channel_cb);
  ssh_set_server_callbacks(session, &server_cb);
  if (ssh_handle_key_exchange(session) != SSH_OK) {
    fprintf(stderr, "%s\n", ssh_get_error(session));
    ssh_disconnect(session);
    ssh_free(session);
    return NULL;
  }
  ssh_event event = ssh_event_new();
  ssh_event_add_session(event, session);
  
  //condizione fine ciclo: auth and channel or timeout or too many auth attempts
  int n=0;
  while (!sdata.authenticated || sdata.client.channel == NULL) {
    if (sdata.auth_attempts >= 3 || n >= 100) {
      ssh_event_free(event);
      ssh_disconnect(session);
      ssh_free(session);
      return NULL;
    }
    if (ssh_event_dopoll(event, 100) == SSH_ERROR) {
      fprintf(stderr, "%s\n", ssh_get_error(session));
      ssh_event_free(event);
      ssh_disconnect(session);
      ssh_free(session);
      return NULL;
    }
    n++;
  }
  
  ssh_set_channel_callbacks(sdata.client.channel, &channel_cb);
  while (!sdata.shellreq || sdata.client.term == NULL) {
    if (ssh_event_dopoll(event, 100) == SSH_ERROR) {
      fprintf(stderr, "%s\n", ssh_get_error(session));
      ssh_event_free(event);
      chClose(&sdata.client);
      return NULL;
    }
  }
  ssh_remove_channel_callbacks(sdata.client.channel, &channel_cb);
  ssh_event_free(event);
  
  pthread_mutex_lock(&clientPool.mutex);
  if(clientPool.terminated) {
    pthread_mutex_unlock(&clientPool.mutex);
    chClose(&sdata.client);
    return NULL;
  }
    
  int i;
  for (i=0; i < clientPool.nclients; i++) {
    if(clientPool.clients[i].channel == NULL) {
      ssh_event_add_session(clientPool.resize_event, session);
      ssh_set_channel_callbacks(sdata.client.channel, &clientPool.callbacks[i]);

      clientPool.clients[i] = sdata.client;
      break;
    }
  }
  
  if(i == clientPool.nclients) {
    chClose(&sdata.client);
  }
  pthread_mutex_unlock(&clientPool.mutex);
  
  return NULL;
}

static void* listening_thread(void *userdata) {
  ssh_bind sshbind = userdata;
  clientPool.terminated = false;
  while(true) {
    ssh_session session = ssh_new();
    if (session == NULL) {
      fprintf(stderr, "Failed to allocate session\n");
      continue;
    }
    
    if(ssh_bind_accept(sshbind, session) != SSH_ERROR) {
      pthread_t tid;
      if (pthread_create(&tid, NULL, polling_thread, session) == 0) pthread_detach(tid);
    }
    else if(clientPool.terminated) break;
    else fprintf(stderr, "%s\n", ssh_get_error(sshbind));
  }
  ssh_bind_free(sshbind);
  return NULL;
}



bool sshStartListening(char* addr, char* port, char* rsa_pk,
		       bool (*user_login_callback)(struct sshclient*, const char*, const char*),
		       void (*user_logout_callback)(struct sshclient*),
		       bool (*resize_callback)(struct sshclient*, int, int)) {
  user_login = user_login_callback;
  user_logout = user_logout_callback;
  resize_handler = resize_callback;
  
  clientPool.nclients = INI_NCLIENTS;
  clientPool.clients = calloc(clientPool.nclients, sizeof(struct sshclient));
  clientPool.callbacks = calloc(clientPool.nclients, sizeof(struct ssh_channel_callbacks_struct));

  for(int i=0; i<clientPool.nclients; i++) {
    clientPool.callbacks[i].userdata = &clientPool.clients[i];
    clientPool.callbacks[i].channel_pty_window_change_function = pty_resize;
    ssh_callbacks_init(&clientPool.callbacks[i]);
  }
  
  if (ssh_init() < 0) {
    fprintf(stderr, "ssh_init failed\n");
    return false;
  }

  ssh_bind sshbind = ssh_bind_new();
  if (sshbind == NULL) {
    fprintf(stderr, "ssh_bind_new failed\n");
    ssh_finalize();
    return false;
  }
  
  ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_BINDADDR, addr);
  ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_BINDPORT_STR, port);
  ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_HOSTKEY, rsa_pk);

  if(ssh_bind_listen(sshbind) < 0) {
    fprintf(stderr, "%s\n", ssh_get_error(sshbind));
    ssh_bind_free(sshbind);
    ssh_finalize();
    return false;
  }
  
  if(pthread_create(&clientPool.listen_tid, NULL, listening_thread, sshbind) != 0) return false;
  return true;
}

void sshClose(void) {
  clientPool.terminated = true;
  //stop accepting new client connections
  pthread_kill(clientPool.listen_tid, SIGINT);
  pthread_join(clientPool.listen_tid, NULL);
  
  //close all connected clients
  struct sshclient *cl = NULL;
  while((cl = nextClient(cl)) != NULL)
    chClose(cl);
  ssh_event_free(clientPool.resize_event);
  ssh_finalize();
  free(clientPool.clients);
  free(clientPool.callbacks);
}

static void reallocPool(size_t nclients) {
  for(int i = 0; i<clientPool.nclients; i++)
    if(clientPool.clients[i].channel != NULL)
      ssh_remove_channel_callbacks(clientPool.clients[i].channel, &clientPool.callbacks[i]);
      
  clientPool.clients = realloc(clientPool.clients, sizeof(struct sshclient[nclients]));
  clientPool.callbacks = realloc(clientPool.callbacks, sizeof(struct ssh_channel_callbacks_struct[nclients]));
      
  for(int i = 0; i< MIN(clientPool.nclients, nclients); i++) {
    clientPool.callbacks[i].userdata = &clientPool.clients[i];
    if(clientPool.clients[i].channel != NULL)
      ssh_set_channel_callbacks(clientPool.clients[i].channel, &clientPool.callbacks[i]);
  }

  for(int i=clientPool.nclients; i<nclients; i++) {
    clientPool.clients[i] = (struct sshclient){};
    clientPool.callbacks[i] = (struct ssh_channel_callbacks_struct) {
      .userdata = &clientPool.clients[i],
      .channel_pty_window_change_function = pty_resize
    };
    ssh_callbacks_init(&clientPool.callbacks[i]);
  }
  clientPool.nclients = nclients;
}

struct sshclient* nextClient(struct sshclient* prev) {
  
  int i;
  if(prev == NULL) i=0;
  else {
    i=(prev+1-clientPool.clients)%clientPool.nclients;
    if(i == 0) {
      fprintf(stderr, "need more client slots: %zu -> %zu\n", clientPool.nclients, clientPool.nclients*2);
      reallocPool(clientPool.nclients*2);
      return NULL;
    }
  }
  
  struct sshclient* client = NULL;
  
  pthread_mutex_lock(&clientPool.mutex);
  for(;i<clientPool.nclients; i++) {
    ssh_channel channel = clientPool.clients[i].channel;
    if(channel != NULL) {
      if(ssh_channel_is_closed(channel) || ssh_channel_is_eof(channel)) {
	chClose(&clientPool.clients[i]);
      } else {
	client = &clientPool.clients[i];
	break;
      }
    }
  }
  pthread_mutex_unlock(&clientPool.mutex);
  
  return client;
}

void chAttach(struct sshclient *client, void *attachment) {
  client->attachment = attachment;
}

void* chGetAttachment(struct sshclient *client) {
  return client->attachment;
}

const char* chGetTerm(struct sshclient *client) {
  return client->term;
}

int chRead(struct sshclient *client, char *dst, int n) {
  return ssh_channel_read_nonblocking(client->channel, dst, n, 0);
}

int chWrite(struct sshclient *client, char *src, int n) {
  return ssh_channel_write(client->channel, src, n);
}

void chClose(struct sshclient *client) {
  user_logout(client);
  ssh_session session = ssh_channel_get_session(client->channel);
  ssh_channel_close(client->channel);
  ssh_disconnect(session);
  ssh_free(session);
  free((void*)client->term);
  client->term = NULL;
  client->channel = NULL;
}

void handleResize(void) {
  ssh_event_dopoll(clientPool.resize_event, 0);
}
