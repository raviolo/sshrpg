struct terminal;

struct terminal* newTerm(const char* type);
void freeTerm(struct terminal*);

void setTerm(struct terminal*);
int sendInput(struct terminal*, char* src, int n);
int getOutput(struct terminal*, char* dst, int n);
