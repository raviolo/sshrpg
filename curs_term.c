#include "curs_term.h"
#include <curses.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

struct terminal {
  int pipeIn[2];
  FILE* termin;
  SCREEN* term;  
  FILE* termout;
  int pipeOut[2];
};

struct terminal* newTerm(const char* type) {
  struct terminal* term = malloc(sizeof(struct terminal));
  pipe(term->pipeIn);
  pipe(term->pipeOut);
  int flags = fcntl(term->pipeOut[0], F_GETFL, 0);
  fcntl(term->pipeOut[0], F_SETFL, flags | O_NONBLOCK);
  
  term->termin = fdopen(term->pipeIn[0], "r");
  term->termout = fdopen(term->pipeOut[1], "w");
  term->term = newterm(type, term->termout, term->termin);
  if(term->term == NULL) {
    freeTerm(term);
    return NULL;
  }
  noecho();
  nodelay(stdscr, true);
  raw();
  return term;
}

void freeTerm(struct terminal* term) {
  close(term->pipeIn[1]);
  fclose(term->termin);
  delscreen(term->term);
  fclose(term->termout);
  close(term->pipeOut[0]);
  free(term);
}

void setTerm(struct terminal* term) {
  set_term(term->term);
}

int sendInput(struct terminal* term, char* src, int n) {
  return write(term->pipeIn[1], src, n);
}

int getOutput(struct terminal* term, char* dst, int n) {
  return read(term->pipeOut[0], dst, n);
}
