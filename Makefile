LDLIBS = -lssh -lncurses -lpthread
CFLAGS = -g -Wall
all: server
server: main.o ssh_serv.o curs_term.o
	cc -o server main.o ssh_serv.o curs_term.o $(LDLIBS)
