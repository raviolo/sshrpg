#include "ssh_serv.h"
#include "curs_term.h"
#include <curses.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/param.h>

struct user {
  bool active;
  char ch;
  int sx,sy,x,y;
  bool freeze;
  int typeCount;

  WINDOW* win;
  int wx,wy;
  bool wresized;
  int wcols, wrows;
  
  struct terminal* term;
};

struct terminal* initTerm(const char* type) {
  struct terminal* term = newTerm(type);
  fprintf(stderr, "new term:%s\n", type);
  setTerm(term);
  start_color();
  init_pair(0, COLOR_WHITE, COLOR_BLACK);
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_YELLOW, COLOR_BLACK);
  init_pair(3, COLOR_CYAN, COLOR_BLACK);
  init_pair(4, COLOR_GREEN, COLOR_BLACK);
  return term;
}

#define BUFSIZE 2048
#define MAXUSERS 64
#define MAXTERMS 32
struct user users[MAXUSERS];

struct{
  const char* type;
  struct terminal* term;
} terms[MAXTERMS];

static bool setupTerm(struct user* user, const char* type) {
  for(int i=0; i<MAXTERMS; i++) {
    if(terms[i].term == NULL) {
      struct terminal* term = initTerm(type);
      if(term == NULL) return false;
      terms[i].term = term;
      terms[i].type = strdup(type);
    }
    if(strcmp(terms[i].type, type) == 0) {
      user->term = terms[i].term;
      setTerm(user->term);
      user->win = newwin(0,0,0,0);
      //a volte ncurses si dimentica che keypad = true, meglio ricordarglielo
      keypad(stdscr, true);
      return true;
    }
  }
  return false;
}

bool user_login(struct sshclient *client, const char* name, const char* pass) {
  if(strcmp(pass, "12345") != 0) {
    fprintf(stderr, "user login failed:%s, p:%s\n", name, pass);
    return false;
  }
  fprintf(stderr, "new user:%s\n", name);
  for(int i=0; i<MAXUSERS; i++) {
    if(!users[i].active) {
      struct user* user = &users[i];
      *user = (struct user){
	.ch = name[0],
	.sx = 1,
	.active = true
      };
      chAttach(client, user);
      return true;
    }
  }
  return false;
}

static int term2channel(struct terminal* term, struct sshclient* client) {
    static char buf[BUFSIZE];
    
    int written = 0;
    while(true) {
      int l = getOutput(term, buf, BUFSIZE);
      if(l <= 0) break;
      written += chWrite(client, buf, l);
    }
    return written;
}

void user_logout(struct sshclient* client) {
  struct user* user = chGetAttachment(client);
  if(user->term != NULL) {
    setTerm(user->term);
    clear();
    wrefresh(user->win);
    term2channel(user->term, client);

    delwin(user->win);
    endwin();
    user->term = NULL;
  }
  user->active = false;
}

bool resize_handler(struct sshclient *client, int rows, int cols) {
  struct user* user = chGetAttachment(client);
  user->wresized = true;
  user->wcols = cols;
  user->wrows = rows;

  return true;
}

static void user_turn_clockwise(struct user* u, int steps) {
  for(int i=0; i<steps; i++) {
    if(u->sy == 0) {
      u->sy = u->sx;
      u->sx = 0;
    } else {
      u->sx = -u->sy;
      u->sy = 0;
    }
  }
}

#define CONFINE(x, lower_b, upper_b) x = MIN(MAX(x, lower_b), upper_b)
static void user_move(struct user* u, int steps) {
  u->x += u->sx * steps;
  u->y += u->sy * steps;
  //non far uscire l'utente dal canvas
  CONFINE(u->x, 0, COLS-1);
  CONFINE(u->y, 0, LINES-1);
  
  //la finestra deve seguire l'utente
  if(u->x >= u->wx+u->wcols) u->wx += u->wcols/2+1;
  if(u->y >= u->wy+u->wrows) u->wy += u->wrows/2+1;
  if(u->x < u->wx) u->wx -= u->wcols/2+1;
  if(u->y < u->wy) u->wy -= u->wrows/2+1;
  //non far uscire i bordi della finestra dal canvas
  CONFINE(u->wx, 0, COLS-u->wcols);
  CONFINE(u->wy, 0, LINES-u->wrows);
  //DEBUG
  //fprintf(stderr, "window_pos(y,x):%d,%d wsize(rxc):%dx%d user_pos(y,x):%d,%d\n", u->wy, u->wx, u->wrows, u->wcols, u->y, u->x);
}

volatile bool terminated = false;
void sigint_handler(int signal) {
  if(signal == SIGINT) terminated = true;
}

#define NAPMS 50
int main(int argc, char* argv[]) {
  if(argc != 4) {
    fprintf(stderr, "usage: %s BINDADDR PORT RSA_PRIVATE_KEY\n", argv[0]);
    return 1;
  }

  struct sigaction sa;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
    
  sa.sa_handler = sigint_handler;
  sigaction(SIGINT, &sa, NULL);

  //questo terminale verrà usato quasi sicuramente. Inoltre, inizializzandolo in anticipo, imposta LINES, COLS e i sig_handlers per SIGINT e SIGWINCH
  terms[0].type = strdup("xterm-256color");
  terms[0].term = initTerm(terms[0].type);

  sshStartListening(argv[1], argv[2], argv[3], user_login, user_logout, resize_handler);

  char** canvas = malloc(sizeof(char*[LINES]));
  for(int i=0; i<LINES; i++) {
    canvas[i] = malloc(sizeof(char[COLS]));
    for(int j=0; j<COLS; j++) {
      canvas[i][j] = ' ';
    }
  }
  int scanline = 0;
  while(!terminated) {
    handleResize();
    struct sshclient *cl = NULL;
    while((cl = nextClient(cl)) != NULL) {
      struct user* user = chGetAttachment(cl);
      if(user->term == NULL && !setupTerm(user, chGetTerm(cl))) {
	  char* msg = "$TERM non supportato\n\r";
	  chWrite(cl, msg, strlen(msg));
	  chClose(cl);
	  continue;
      }
      setTerm(user->term);
      
      char buf[BUFSIZE];
      int l = chRead(cl, buf, BUFSIZE);
      sendInput(user->term, buf, l);

      if(user->wresized) {
	user->wresized = false;

	user->wrows = MIN(user->wrows, LINES);
	user->wcols = MIN(user->wcols, COLS);
	// tieni user dentro la finestra dopo resize--
	//non far uscire i bordi della finestra dal canvas
	CONFINE(user->wx, user->x-user->wcols+1, COLS-user->wcols);
	CONFINE(user->wy, user->y-user->wrows+1, LINES-user->wrows);

	//fprintf(stderr, "%p resized:%dx%d\n", user, user->wrows, user->wcols);
	wresize(user->win, user->wrows, user->wcols);
      }

      bool quitting = false;
      int ch;
      while((ch = getch()) != ERR) {
	switch(ch) {
	case KEY_LEFT:
	  user->typeCount = 0;
	  if(user->sx != -1) {
	    user->sx = -1;
	    user->sy = 0;
	  }
	  else user_move(user, 1);
	  break;
	case KEY_DOWN:
	  user->typeCount = 0;
	  if(user->sy != 1) {
	    user->sx = 0;
	    user->sy = 1;
	  }
	  else user_move(user, 1);
	  break;
	case KEY_UP:
	  user->typeCount = 0;
	  if(user->sy != -1) {
	    user->sx = 0;
	    user->sy = -1;
	  }
	  else user_move(user, 1);
	  break;
	case KEY_RIGHT:
	  user->typeCount = 0;
	  if(user->sx != 1) {
	    user->sx = 1;
	    user->sy = 0;
	  }
	  else user_move(user, 1);
	  break;
	case KEY_BACKSPACE:
	  if(user->typeCount > 0) {
	    user->typeCount--;
	    user_move(user, -1);
	    canvas[user->y][user->x] = ' ';
	  }
	  break;
	case '\n':
	  user_move(user, -user->typeCount);
	  user->typeCount = 0;
	  user_turn_clockwise(user,1);
	  user_move(user, 1);
	  user_turn_clockwise(user,3);
	  break;
	case '\t':
	  user->freeze = !user->freeze;
	  if(user->freeze) {
	    wcolor_set(user->win, 0, NULL);
	    clear();
	    for(int i=0; i<user->wrows; i++)
	      mvwaddnstr(user->win, i, 0, canvas[user->wy+i]+user->wx, user->wcols);
	    wrefresh(user->win);
	    term2channel(user->term, cl);
	  }
	  break;
	case 27:
	  quitting = true;
	  break;
	default:
	  if(' ' <= ch && ch <= '~') {
	    canvas[user->y][user->x] = ch;
	    user_move(user, 1);
	    user->typeCount++;
	  }
	}
      }
      if(quitting) {
	chClose(cl);
	continue;
      }

      if(user->freeze) continue;
      
      wcolor_set(user->win, 0, NULL);
      clear();
      
      for(int i=0; i<user->wrows; i++) {
	if(i == scanline) wcolor_set(user->win, 4, NULL);
	if(i == scanline+3) wcolor_set(user->win, 0, NULL);
	mvwaddnstr(user->win, i, 0, canvas[user->wy+i]+user->wx, user->wcols);
      }
      
      wcolor_set(user->win, 3, NULL);
      for(int i=0; i<MAXUSERS; i++) {
	struct user* u = &users[i];
	if(u->active) mvwaddch(user->win, u->y-user->wy, u->x-user->wx, u->ch);
      }

      wcolor_set(user->win, 1, NULL);
      mvwaddch(user->win, user->y-user->wy, user->x-user->wx, user->ch);
      int dirch;
      if(user->sy == 0) dirch = user->sx == 1 ? '>' : '<';
      else dirch = user->sy == 1 ? 'v' : '^';
      wcolor_set(user->win, 2, NULL);
      mvwaddch(user->win, user->y+user->sy-user->wy, user->x+user->sx-user->wx, dirch);
      wmove(user->win, 0,0);
      wrefresh(user->win);
      
      term2channel(user->term, cl);
    }
    scanline = (scanline+1)%LINES;

    
    static clock_t start=0;
    clock_t elapsed = (clock() - start) * 1000 / CLOCKS_PER_SEC;
    //fprintf(stderr, "%ld\n", elapsed);
    if(NAPMS - elapsed > 0)
      napms(NAPMS - elapsed);
    start = clock();
    
  }

  sshClose();
  for(int i = 0; i<MAXTERMS; i++) {
    if(terms[i].term != NULL) {
      freeTerm(terms[i].term);
      
      free((void*)terms[i].type);
    } else break;
  }

  for(int i=0; i<LINES; i++)
    free(canvas[i]);
  free(canvas);
  fputs("terminating...\n", stderr);
}
