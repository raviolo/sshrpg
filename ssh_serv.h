#include <stdbool.h>
struct sshclient;

bool sshStartListening(char* addr, char* port, char *rsa_pk,
		       bool (*user_login)(struct sshclient*, const char* name, const char* pass),
		       void (*user_logout)(struct sshclient*),
		       bool (*resize_handler)(struct sshclient*, int rows, int cols));
void sshClose(void);

struct sshclient* nextClient(struct sshclient* prev);

void chAttach(struct sshclient*, void *attachment);
void* chGetAttachment(struct sshclient*);
const char* chGetTerm(struct sshclient*);
int chRead(struct sshclient*, char *dst, int n);
int chWrite(struct sshclient*, char *src, int n);
void chClose(struct sshclient*);
void handleResize(void);
